//const planvelo = require('./paris_plan_v_lo_2026.json');
const planvelo = require('./map_20221006T1905.json');
planvelo.features = planvelo.features.filter(feature => (feature.properties['Etat'] != "Réalisé Pré-2021") && (feature.properties['Etat'] != "Hors Plan Vélo (Embellir)"));
var turf = require('@turf/turf');
//import { planvelo } from './paris_plan_v_lo_2026.json';
// or in ES6
var totalLength = 0;
for (const feature of planvelo.features) {
    console.log(feature.geometry);
    totalLength += turf.length(feature, {units: "kilometers"});
}
console.log(totalLength, 'km');
